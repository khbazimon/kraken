﻿Shader "Roystan/Toon Complete With Normal and caustics"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Main Texture", 2D) = "white" {}
		_MainNormal("Main Normal", 2D) = "white" {}
		_NormalStrength("Normal Strength", Float) = 1
		_MainSpecular("Main Specular", 2D) = "white" {}
		// Ambient light is applied uniformly to all surfaces on the object.
		[HDR]
		_AmbientColor("Ambient Color", Color) = (0.4,0.4,0.4,1)
		[HDR]
		_SpecularColor("Specular Color", Color) = (0.9,0.9,0.9,1)
		// Controls the size of the specular reflection.
		_Glossiness("Glossiness", Float) = 32
		[HDR]
		_RimColor("Rim Color", Color) = (1,1,1,1)
		_RimAmount("Rim Amount", Range(0, 1)) = 0.716
		// Control how smoothly the rim blends when approaching unlit
		// parts of the surface.
		_RimThreshold("Rim Threshold", Range(0, 1)) = 0.1		

		_OutlineColor("Outline Color", Color) = (0,0,0,1)
		_OutlineThickness ("Outline Thickness", Range(0,1)) = 0.1
		
		
		_CausticsColor("Caustics Color", Color) = (1,1,1,1)
		_CausticDepth("Caustic Depth", Float) = 4

		[Toggle(OUTLINE_ON)]
        _FillWithRed ("Use Outline", Float) = 0
		

	}
	SubShader
	{
		Pass
		{
			// Setup our pass to use Forward rendering, and only receive
			// data on the main directional light and ambient light.
			Tags
			{
				"LightMode" = "ForwardBase"
				"PassFlags" = "OnlyDirectional"
			}

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// Compile multiple versions of this shader depending on lighting settings.
			#pragma multi_compile_fwdbase
			
			#pragma shader_feature OUTLINE_ON

			#include "UnityCG.cginc"
			// Files below include macros and functions to assist
			// with lighting and shadows.
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;				
				float4 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 worldPos : TEXCOORD4;
				float3 worldNormal : NORMAL;
				float2 uv : TEXCOORD0;
				float3 viewDir : TEXCOORD1;	
				float3 normalDir : TEXCOORD2;
				float3 objectNormal : TEXCOORD3;
				
				// Macro found in Autolight.cginc. Declares a vector4
				// into the TEXCOORD2 semantic with varying precision 
				// depending on platform target.
				SHADOW_COORDS(2)
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			sampler2D _MainNormal;
			float4 _MainNormal_ST;

			sampler2D _MainSpecular;
			float4 _MainSpecular_ST;

			float _NormalStrength;
			
			float4 _OutlineColor;
			float _OutlineThickness;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul (unity_ObjectToWorld, v.vertex);
				o.worldNormal = UnityObjectToWorldNormal(v.normal);		
				o.objectNormal = v.normal;		
				o.viewDir = WorldSpaceViewDir(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				// Defined in Autolight.cginc. Assigns the above shadow coordinate
				// by transforming the vertex from world space to shadow-map space.
				TRANSFER_SHADOW(o)
				return o;
			}
			
			float4 _Color;

			float4 _AmbientColor;

			float4 _SpecularColor;
			float _Glossiness;		

			float4 _RimColor;
			float _RimAmount;
			float _RimThreshold;	


			float _Lux_UnderWaterCausticsScale;
			float _Lux_UnderWaterCausticsSpeed;
			float _Lux_UnderWaterCausticsTiling;
			float _Lux_UnderWaterCausticsSelfDistortion;
			float2 _Lux_UnderWaterFinalBumpSpeed01;

				fixed3 _Lux_UnderWaterSunColor;
	float3 _Lux_UnderWaterSunDir;
	fixed3 _Lux_UnderWaterAmbientSkyLight;


		float  _Lux_UnderWaterWaterSurfacePos;
	float  _Lux_UnderWaterDirLightingDepth;
	float  _Lux_UnderWaterFogLightingDepth;

			sampler2D _Lux_UnderWaterCaustics;



			half4 _CausticsColor;
			float _CausticDepth;

			float4 frag (v2f i) : SV_Target
			{

				float4 normalSample = tex2D(_MainNormal, i.uv);
				float4 specularSample = tex2D(_MainSpecular, i.uv);

				float3 normal = normalize(i.worldNormal) + (normalSample.xyz * _NormalStrength);
				float3 viewDir = normalize(i.viewDir);

				// Lighting below is calculated using Blinn-Phong,
				// with values thresholded to creat the "toon" look.
				// https://en.wikipedia.org/wiki/Blinn-Phong_shading_model

				// Calculate illumination from directional light.
				// _WorldSpaceLightPos0 is a vector pointing the OPPOSITE
				// direction of the main directional light.
				float NdotL = dot(_WorldSpaceLightPos0, normal);

				// Samples the shadow map, returning a value in the 0...1 range,
				// where 0 is in the shadow, and 1 is not.
				float shadow = SHADOW_ATTENUATION(i);
				// Partition the intensity into light and dark, smoothly interpolated
				// between the two to avoid a jagged break.
				float lightIntensity = smoothstep(0, 0.01, NdotL * shadow);	
				// Multiply by the main directional light's intensity and color.
				float4 light = lightIntensity * _LightColor0;

				// Calculate specular reflection.
				float3 halfVector = normalize(_WorldSpaceLightPos0 + viewDir);
				float NdotH = dot(normal, halfVector);
				// Multiply _Glossiness by itself to allow artist to use smaller
				// glossiness values in the inspector.
				float specularIntensity = pow(NdotH * lightIntensity, length(specularSample)*_Glossiness);
				float specularIntensitySmooth = smoothstep(0.005, 0.01, specularIntensity);
				float4 specular = specularIntensitySmooth * specularSample;				

				// Calculate rim lighting.
				float rimDot = 1 - dot(viewDir, normal);
				// We only want rim to appear on the lit side of the surface,
				// so multiply it by NdotL, raised to a power to smoothly blend it.
				float rimIntensity = rimDot * pow(NdotL, _RimThreshold);
				rimIntensity = smoothstep(_RimAmount - 0.01, _RimAmount + 0.01, rimIntensity);
				float4 rim = rimIntensity * _RimColor;

				float4 sample = tex2D(_MainTex, i.uv);

				float outlineStrength = saturate( (dot(i.worldNormal, normalize(i.viewDir) ) - (_OutlineThickness)) * 1000.0f );

				float4 finalColor = (light + _AmbientColor + specular + rim) * _Color * sample;




				float2 cTexUV = i.worldPos.xz * _Lux_UnderWaterCausticsTiling;
				half4 caustics = tex2D(_Lux_UnderWaterCaustics, cTexUV + _Time.x * 0.5 );
				//	animate			
				float CausticsTime = _Time.x * _Lux_UnderWaterCausticsSpeed;


				float causticsDepthBelowSurface = saturate( ( _Lux_UnderWaterWaterSurfacePos - i.worldPos.y) / _Lux_UnderWaterDirLightingDepth);
				float causticsDepthBelowSurface1 = exp2(-causticsDepthBelowSurface * causticsDepthBelowSurface * _CausticDepth);
				float dirLightCancellation = saturate( causticsDepthBelowSurface1 - causticsDepthBelowSurface * 0.01);
				
			
				caustics +=  tex2D(_Lux_UnderWaterCaustics, cTexUV + CausticsTime.xx * _Lux_UnderWaterFinalBumpSpeed01.xy);
				caustics += tex2D(_Lux_UnderWaterCaustics, cTexUV * 0.78 + float2(-CausticsTime, -CausticsTime * 0.87) + caustics.gb * 0.1 );
				caustics += tex2D(_Lux_UnderWaterCaustics, cTexUV * 1.13 + float2(CausticsTime, 0.36) - caustics.gb * _Lux_UnderWaterCausticsSelfDistortion );
		
				caustics.r *= saturate(normal.y - 0.15) * saturate( (normal.y - 0.15) * 2);
				caustics /= 2.0f;
				caustics *= _CausticsColor;
				float3 causticsMask = dirLightCancellation;

				finalColor.xyz += caustics.rrr * causticsMask * _Lux_UnderWaterCausticsScale * _Lux_UnderWaterSunColor.rgb;

				//return float4(c.x, c.y, c.z, 1.0f);

				#if OUTLINE_ON
				//Some code here.
					
					return lerp(_OutlineColor, finalColor, outlineStrength);
				#endif

				return finalColor;
			}
			ENDCG
		}

		// Shadow casting support.
        UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
	}
}