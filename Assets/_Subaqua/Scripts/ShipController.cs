﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour {

    public float speed_kmph = 0;
    private float speed_mps;
    [Range(-1, 1)]
    public float roll;
    [Range(-1, 1)]
    public float yaw;
    public float turnSpeed_yaw = 0.01f;

    [Header("Maximum Values")]
    public float maxSpeed_kmph = 100;
    private float maxSpeed_mps;
    [Range(0, 360)]
    public float maxYaw = 45; // Left Right Turn

    [Range(0, 360)]
    public float maxRoll = 20; // Left Right Tilt

    [Header("Parameters")]
    public Transform shipVisuals;
    public Transform shipCamera;

    public bool isUserControlled = false;
    public bool canTiltWithMouse = false;
    private float tempLerpTime = 0;

	// Use this for initialization
	void Start () {
        //Cursor.lockState = CursorLockMode.Locked;
    }
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.C))
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

    //}

    //private void LateUpdate()
    //{
        
        if (isUserControlled)
            GetInput();
        {
            //RotateShip
            shipVisuals.localRotation = Quaternion.Euler(new Vector3(0, yaw * maxYaw, ((EaseMid(roll*0.5f)* (canTiltWithMouse?1:0)) + 0.5f*(-Input.GetAxis("Turn")) * maxRoll)));
            shipCamera.localRotation = Quaternion.Euler(new Vector3(0, yaw * maxYaw, 0));
            //MoveShip
            if (speed_kmph > maxSpeed_kmph)
            {
                speed_kmph = maxSpeed_kmph;
            }
            maxSpeed_mps = maxSpeed_kmph / 3.6f;
            speed_mps = speed_kmph / 3.6f;
            transform.position = transform.position + (Quaternion.Euler(0, yaw * maxYaw, 0) * (Vector3.forward * speed_mps * Time.deltaTime));
        }
    }
    private void GetInput()
    {
        roll += Input.GetAxis("Tilt");
        yaw += Input.GetAxis("Turn") * turnSpeed_yaw * Time.deltaTime;
        speed_kmph += Input.GetAxis("Speed") * 10;
        if(Input.GetAxis("Brake") != 0)
        {
            tempLerpTime += Time.deltaTime;
            speed_kmph = Mathf.Lerp(speed_kmph, 0, tempLerpTime);
        }
        else
        {
            tempLerpTime = 0;
        }
    }

    private void OnValidate()
    {
        if (speed_kmph > maxSpeed_kmph)
        {
            speed_kmph = maxSpeed_kmph;
        }
    }

     

    /// <summary>
    /// Makes the ship turn left and right as if it is trying to move left or right.
    /// </summary>
    /// <param name="yaw">The amount for the ship to turn</param>
    public void AddYaw(float yaw)
    {
        //transform.Rotate(Vector3.up * (Mathf.Clamp(transform.localRotation.y + yaw, -maxRoll, maxRoll) - transform.localRotation.y));
        if(Mathf.Sign(yaw) > 0)
        {// If yaw is positive
            //transform.Rotate(Vector3.up * (Mathf.Min())
            
        }
        else
        {//If yaw is negative
            //transform.Rotate(Vector3.up * (Mathf.Max())
        }
    }

    /// <summary>
    /// Makes the ship tilt left and right as if it could be capsized.
    /// </summary>
    /// <param name="roll">The amount for the ship to roll/tilt</param>
    public void AddRoll(float roll)
    {
        //transform.Rotate(Vector3.forward * roll);
        
    }

    /// <summary>
    /// Eases in the middle of the line (0)
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    private float EaseMid(float x)
    {
        x = Mathf.Clamp(x, -1, 1);
        return Mathf.Sign(x) * (Mathf.Sin(Mathf.PI / 2 * (x - 1)) + 1f);
    }
}
