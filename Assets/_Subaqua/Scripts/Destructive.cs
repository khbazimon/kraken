﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Destructive : MonoBehaviour
{
    private bool destroyed;
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        destroyed = false;
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeAll;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        rb.AddForce(new Vector3(0, 2f * Time.deltaTime, 0));
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag != "Destructive")
        {
            rb.constraints = RigidbodyConstraints.None;
            //this.gameObject.tag = "Destroyed";
            //rb.AddForceAtPosition(collision.collider.GetComponent<Rigidbody>().velocity, collision.collider.transform.position);

        }
    }
}
