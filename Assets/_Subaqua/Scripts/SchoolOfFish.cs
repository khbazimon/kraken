﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SchoolOfFish : MonoBehaviour {

    [Header("Fish")]
    public GameObject fish;
    public int minFishAmount= 5;
    public int maxFishAmount= 10;

    private int fishAmount;

    private List<GameObject> fishes;
    private Vector3 fishAim = Vector3.zero;

    [Header("School")]
    public float minDelay = 0;
    public float maxDelay = 7;
    public float size = 1;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position, new Vector3(size*2, size*2, size*2));
        Gizmos.color = Color.green;
        if (fishAim != Vector3.zero)
        {
            Gizmos.DrawSphere(fishAim, 1);
        }
    }

    // Use this for initialization
    void Start () {
        fishAmount = Random.Range(maxFishAmount, minFishAmount);
        fishes = new List<GameObject>();

        for (int i = 0; i < fishAmount; ++i)
        {
            fishes.Add(Instantiate(fish, this.transform));
            if(fishes[i].GetComponent<Fish>() == null)
            {
            fishes[i].AddComponent<Fish>();
            }
            fishes[i].GetComponent<Fish>().school = this;
            fishes[i].transform.position = transform.position + new Vector3(Random.Range(-size, size), Random.Range(-size, size), Random.Range(-size, size));
        }
        StartCoroutine(ChangeFishAim());
    }
	
	// Update is called once per frame
	void Update () {
        
    }
    
    IEnumerator ChangeFishAim()
    {
        fishAim = new Vector3(Random.Range(-size, size), Random.Range(-size, size), Random.Range(-size, size)) + transform.position;
        yield return new WaitForSeconds(Random.Range(minDelay, maxDelay));
        StartCoroutine(ChangeFishAim());
    }

    public List<GameObject> GetFishes()
    {
        return fishes;
    }

    public Vector3 GetFishAim()
    {
        return fishAim;
    }
}
