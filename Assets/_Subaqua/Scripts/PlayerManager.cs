﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.Networking;
using System;
public class PlayerManager : NetworkBehaviour {

    [Header("Player Prefabs")]
    public GameObject daPlayer;
    public GameObject vrPlayer;

    [SerializeField]
    public PlayerType playerType;

    [SerializeField]
    private PlayerControls playerControls;

    private PlayerType _playerType;
    private PlayerControls _playerControls;

    private static List<NetworkPlayer> players = new List<NetworkPlayer>();
    public List<NetworkPlayer> playersVis;
    [SerializeField]
    private GameObject localPlayer;

    private bool isPlayerCreated = false;

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update()
    {
        if (isLocalPlayer && !isPlayerCreated)
            playersVis = players;
        {
            KeyboardInput();
            if (Application.isEditor)
            {
                if (_playerType != playerType)
                {
                    SetPlayerType(playerType);
                }
                if (_playerControls != playerControls)
                {
                    SetPlayerControls(playerControls);
                }
            }
        }
    }

    private void KeyboardInput()
    {
        if (Input.GetKeyDown(KeyCode.V) || Input.GetKeyDown(KeyCode.R))
        {
            SetPlayerControls(PlayerControls.VR);
            SetPlayerType(PlayerType.Creature);
        }
        else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.A))
        {
            SetPlayerControls(PlayerControls.DataArena);
            SetPlayerType(PlayerType.ShipCrew);
        }
        if (Input.GetKeyDown(KeyCode.Return) && localPlayer == null)
        {
            Cmd_SpawnPlayer();
        }
    }

    public void SetPlayerControls(PlayerControls playerControls)
    {
        //If a VR Headset is plugged in force VR to stop game from crashing
        if (SteamVR.instance != null)
        {
            _playerControls = PlayerControls.VR;
            this.playerControls = PlayerControls.VR;
            return;
        }
        else
        {
            _playerControls = playerControls;
            this.playerControls = playerControls;
        }
    }

    public void SetPlayerType(PlayerType playerType)
    {
        this.playerType = playerType;
        _playerType = playerType;
    }

    [Command]
    private void Cmd_SpawnPlayer() {
        switch (playerControls)
        {
            case PlayerControls.DataArena:
                localPlayer = Instantiate(daPlayer);
            break;
            case PlayerControls.VR:
                localPlayer = Instantiate(vrPlayer);
                break;
        }
        switch (playerType)
        {
            case PlayerType.Creature:
                localPlayer.AddComponent<Creature>().playerControls = playerControls;
                break;
            case PlayerType.ShipCrew:
                localPlayer.AddComponent<ShipCrew>().playerControls = playerControls;
                try
                {
                    localPlayer.transform.parent = GameObject.FindGameObjectWithTag("Ship").transform;
                }
                catch (NullReferenceException e)
                {
                    Debug.LogError("GameObject with tag \"Ship\" not found. Can't Attach ShipCrew to it.");
                }
                break;
        }
        if (localPlayer.GetComponent<NetworkPlayer>() == null)
        {
            players.Add(localPlayer.AddComponent<NetworkPlayer>());
            localPlayer.GetComponent<NetworkPlayer>().InstantiatePlayer(playerType, playerControls, isLocalPlayer);
        }
        NetworkServer.SpawnWithClientAuthority(localPlayer, connectionToClient);
        isPlayerCreated = true;
    }

    public static List<Vector3> GetShipPlayersPosition()
    {
        List<Vector3> playerPos = new List<Vector3>();
        foreach(NetworkPlayer player in players)
        {
            //playerPos.Add(player.GetPlayerPosition());
        }
        return playerPos;
    }
}
