﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam : MonoBehaviour
{
    public Transform follow;
    void LateUpdate()
    {
        transform.Translate((follow.position - transform.position) * (0.8f * Time.deltaTime), Space.World);
        transform.LookAt(follow);

    }
}
