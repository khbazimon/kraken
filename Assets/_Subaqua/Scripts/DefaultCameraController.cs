﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultCameraController : MonoBehaviour {

    public enum CameraView { VR, DataArena };
    public CameraView currentCamera;
    [Space]
    public GameObject vrObj;
    public GameObject dataArenaObj;
	
	
	// Update is called once per frame
	void Update () {
        switch (currentCamera)
        {
            case (CameraView.VR):
                vrObj.SetActive(true);
                dataArenaObj.SetActive(false);
                break;
            case (CameraView.DataArena):
                //Avoid Crash by Turning Off DataArena when VR is open
                if (Valve.VR.SteamVR.instance == null) {
                    vrObj.SetActive(false);
                    dataArenaObj.SetActive(true);
                } else{
                    currentCamera = CameraView.VR;
                    Debug.LogError("Turning on Data Arena Camera in VR Viewport will crash program. Forcing VR View.");
                }
                break;
        }

    }
}
