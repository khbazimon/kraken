﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class ShipTilt : NetworkBehaviour
{

    public Transform boundingCenter;
    public float boundingHeight;
    public float boundingRadius;

    public GameObject ship;
    public Vector2 currentTiltDegrees = Vector3.zero;
    public float maxTilt = 25f;
    
    private float exponential = 1.5f;

    //Used in AveragePositionOfAllPlayers
    private Vector2 averagePosition;
    private int playerCount;
    private Quaternion temp;
    // Start is called before the first frame update
    void Start()
    {
        currentTiltDegrees = Vector2.zero;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        DebugExtension.DrawCylinder(boundingCenter.position, (boundingCenter.position + (Vector3.up * boundingHeight)), boundingRadius);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(NetworkServer.connections.Count > 0)
        {
            ship.transform.localRotation = Quaternion.Euler(0/*GetTilt().y*/, 0, -GetTilt().x);
        }
    }

    Vector2 PositionWithinBounds(Vector3 playerPos)
    {
        Vector3 vec = (playerPos - boundingCenter.position)/boundingRadius;
        return Vector2.ClampMagnitude((Vector2.right * vec.x) + (Vector2.up *vec.z), 1);
    }

    Vector2 ExponentialPositionWithinBounds(Vector3 playerPos)
    {
        Vector2 temp = PositionWithinBounds(playerPos);
        return new Vector2(Mathf.Sign(temp.x) * Mathf.Pow(Mathf.Abs(temp.x), exponential), Mathf.Sign(temp.y) * Mathf.Pow(Mathf.Abs(temp.y), exponential));
    }

    Vector2 AveragePositionOfAllPlayers()
    {
        averagePosition = Vector3.zero;
        playerCount = 0;
        //////////////////////////////////////////////////////////////////////////// WILL NEED TO BE REPLACED WITH A FOREACH ON ALL TRACKERS OF PLAYERS IN THE DATA ARENA
        foreach (Vector3 position in PlayerManager.GetShipPlayersPosition())
        {
            averagePosition += ExponentialPositionWithinBounds(position);
            ++playerCount;
        }
        averagePosition /= playerCount;
        return averagePosition;
    }

    Vector2 GetTilt()
    {
        if(float.IsNaN(currentTiltDegrees.x) && float.IsNaN(currentTiltDegrees.y))
        {
            currentTiltDegrees = Vector3.zero;
        }
        currentTiltDegrees += AveragePositionOfAllPlayers();
        return currentTiltDegrees;
    }

    Vector2 GetTiltWithCompensation()
    {
        return GetTilt();
    }

}
