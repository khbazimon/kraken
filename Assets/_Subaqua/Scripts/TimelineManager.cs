﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
[RequireComponent(typeof(PlayableDirector))]
public class TimelineManager : MonoBehaviour
{
    PlayableDirector timeline;
    public bool hasPlayed = false;
    // Start is called before the first frame update
    void Start()
    {
        timeline = GetComponent<PlayableDirector>();
        timeline.Pause();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse2) && !hasPlayed)
        {
            hasPlayed = true;
            timeline.Play();
        }
    }
}
