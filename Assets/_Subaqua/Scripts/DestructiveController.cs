﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(Collider))]
public class DestructiveController : MonoBehaviour
{

    public GameObject destructiveShards;
    // Start is called before the first frame update
    void Start()
    {
        destructiveShards.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Destructive" && other.tag != "Destroyed")
        {
            destructiveShards.SetActive(true);
            GetComponent<MeshRenderer>().enabled = false;
            foreach(Collider col in GetComponents<Collider>())
            {
                col.enabled = false;
            }
        }
    }
}
