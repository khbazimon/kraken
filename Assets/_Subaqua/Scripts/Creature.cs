﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class Creature : MonoBehaviour {
    public PlayerControls playerControls;
    public float movementSpeed = 10f;
    //public PlayerControls playerControls;
    public Transform leftHand;
    public Transform rightHand;
    public Transform head;

    private bool touchpadClickToggle = false;
    private Vector3 lastTouchInput;
    Vector3 movementVelocity;
    // Use this for initialization
    void Start () {
        transform.position = new Vector3(40, 17, 78);
        transform.localScale = new Vector3(60, 60, 60);
	}
	
	// Update is called once per frame
	void LateUpdate () {

        //Vector3 lh = (leftHand.rotation * Vector3.forward);
        //Vector3 rh = (rightHand.rotation * Vector3.forward);
        //if (leftHand.rotation.eulerAngles.x > 50 && leftHand.rotation.eulerAngles.x < 70)
        //{
        //    transform.position += (movementSpeed * (head.rotation * Vector3.forward)* Time.deltaTime );
        //}
        Input();
    }


    void Input()
    {
        //if (SteamVR_Actions._default.TouchpadClick[SteamVR_Input_Sources.RightHand].stateDown)
        //{
        //    touchpadClickToggle = !touchpadClickToggle;
        //    Debug.Log(touchpadClickToggle);
        // }

        //if (SteamVR_Actions._default.TouchpadTouch[SteamVR_Input_Sources.RightHand].state || touchpadClickToggle)
        //{
        //touchInput = (Vector3.right * touchInput.x) + (Vector3.forward * touchInput.y);
        movementVelocity = rightHand.rotation * Vector3.forward * Time.deltaTime * movementSpeed;

        //Vector3 touchInput = SteamVR_Actions._default.TouchpadPosition[SteamVR_Input_Sources.RightHand].axis;
        //touchInput = (Vector3.right * touchInput.x) + (Vector3.forward * Mathf.InverseLerp(-1, 1, touchInput.y));
        //touchInput = rightHand.rotation * touchInput * movementSpeed * Time.deltaTime;
        //if (movement == Vector3.zero)
        //{
        //    movement = lastTouchInput;
        //}
        //else
        //{
        //    lastTouchInput = movement;
        //}
        movementVelocity *= (SteamVR_Actions._default.Trigger[SteamVR_Input_Sources.RightHand].axis);

        transform.position += movementVelocity;
        //}
        //float rHandTrigger = (SteamVR_Actions._default.Trigger[SteamVR_Input_Sources.RightHand].axis);
        //float lHandTrigger = (SteamVR_Actions._default.Trigger[SteamVR_Input_Sources.LeftHand].axis);
        //Vector3 movement = Vector3.forward * (rHandTrigger * lHandTrigger)/2 * Time.deltaTime * movementSpeed;
        //if ((SteamVR_Actions._default.Trigger[SteamVR_Input_Sources.RightHand].axis) > (SteamVR_Actions._default.Trigger[SteamVR_Input_Sources.LeftHand].axis))
        //{
            //
        //}
        //else
        //{
            //movement = leftHand.rotation * movement * (SteamVR_Actions._default.Trigger[SteamVR_Input_Sources.LeftHand].axis);
        //}
        //movement = Quaternion.Euler((rightHand.rotation.eulerAngles * (SteamVR_Actions._default.Trigger[SteamVR_Input_Sources.RightHand].axis))/2 + (leftHand.rotation.eulerAngles * (SteamVR_Actions._default.Trigger[SteamVR_Input_Sources.LeftHand].axis))/2) * movement;
        //Debug.Log(movement) ;
        //movement = rightHand.rotation * Vector3.forward * Time.deltaTime * movementSpeed;
        //movement = Quaternion.Euler((rightHand.rotation.eulerAngles * (SteamVR_Actions._default.Trigger[SteamVR_Input_Sources.RightHand].axis)) / 2 + (leftHand.rotation.eulerAngles * (SteamVR_Actions._default.Trigger[SteamVR_Input_Sources.LeftHand].axis)) / 2) * movement;
        //transform.position += movement;
    }

    public Vector3 GetVelocity()
    {
        return movementVelocity;
    }

}
