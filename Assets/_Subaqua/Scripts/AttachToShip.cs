﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachToShip : MonoBehaviour
{
    public GameObject ship;
    public bool attach = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (attach)
        {
            Attach();
        }
    }

    public void Attach()
    {
        transform.parent = ship.transform;
        Destroy(this);
    }
}
