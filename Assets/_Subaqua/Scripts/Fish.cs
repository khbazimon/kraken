﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour {
    public SchoolOfFish school;

    public float minSpeed = 0.5f;
    public float maxSpeed = 1;
    private float speed;
    public float rotationSpeed = 5.0f;
    Vector3 averageHeading;
    Vector3 averagePosition;
    public float avoidDistance = 1f;
    public float neighbourDistance = 10f;
    public float minSize = 5;
    public float maxSize = 8;

    [Header("Particles")]
    public GameObject FishChunks;

    bool turning = false;


	// Use this for initialization
	void Start () {
        float size = Random.Range(minSize, maxSize);
        transform.localScale = new Vector3(size, size, size);
        speed = Random.Range(minSpeed, maxSpeed);
        turning = false;
    }
	
	// Update is called once per frame
	void Update () {
        turning = (Vector3.Distance(transform.position, school.transform.position) >= school.size);
        if (turning)
        {
            Vector3 direction = school.transform.position - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
            speed = Random.Range(minSpeed, maxSpeed);
        }
        else
        {
            if (Random.Range(0, 5) < 1)
            {
                ApplyRules();
            }
        }
        transform.Translate(0, 0, Time.deltaTime * speed);
	}

    void ApplyRules()
    {
        float dist;
        float averageGroupSpeed = 0.1f;
        Vector3 center = Vector3.zero;
        Vector3 avoid = Vector3.zero;
        int groupSize = 0;

        foreach(GameObject fish in school.GetFishes())
        {
            if (fish != this.gameObject)
            {
                dist = Vector3.Distance(fish.transform.position, this.transform.position);
                if(dist <= neighbourDistance)
                {
                    center += fish.transform.position;
                    ++groupSize;
                    if(dist < avoidDistance)
                    {
                        avoid += this.transform.position - fish.transform.position;
                    }

                    averageGroupSpeed += fish.GetComponent<Fish>().speed;
                }
            }
        }

        if(groupSize > 0)
        {
            center = center / groupSize + (school.GetFishAim() - this.transform.position);
            speed = averageGroupSpeed / groupSize;

            Vector3 direction = (center + avoid) - transform.position;
            if(direction != Vector3.zero)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Monster")
        {
            gameObject.SetActive(false);
            Instantiate(FishChunks, transform.position, Quaternion.identity);
        }
    }
}
