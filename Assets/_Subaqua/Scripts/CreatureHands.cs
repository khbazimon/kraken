﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
[RequireComponent(typeof(Animator))]
public class CreatureHands : MonoBehaviour
{
    public SteamVR_Input_Sources hand;//gives us a way to select what hand our script is attached to.
    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    void Update()//update our animator values
    {
        anim.SetFloat("Trigger", 1 -SteamVR_Actions._default.Trigger[hand].axis);
    }
}
