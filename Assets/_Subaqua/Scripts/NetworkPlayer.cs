﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerType { ShipCrew, Creature };
public enum PlayerControls { DataArena, VR }

public class NetworkPlayer : MonoBehaviour {
    [SerializeField]
    public PlayerType playerType;

    [SerializeField]
    public PlayerControls playerControls;


    private List<Camera> cameras = new List<Camera>();

    public bool isLocalPlayer
    {
        set
        {
            isLocalPlayer = value;
            if (value)
            {
                HideCameras();
            }
            else
            {
                ShowCameras();
            }
        }
    }

	void Awake () {
        cameras.AddRange(GetComponentsInChildren<Camera>());
	}

	// Update is called once per frame
	void Update () {
		
	}

    public void InstantiatePlayer(PlayerType type, PlayerControls controls, bool isLocalPlayer)
    {
        this.playerType = type;
        this.playerControls = controls;
        this.isLocalPlayer = isLocalPlayer;
    }

    public void HideCameras()
    {
        foreach(Camera cam in cameras)
        {
            cam.enabled = false;
        }
    }

    public void ShowCameras()
    {
        foreach (Camera cam in cameras)
        {
            cam.enabled = true;
        }
    }
}
