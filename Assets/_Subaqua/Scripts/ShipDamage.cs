﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class ShipDamage : MonoBehaviour
{
    public GameObject explosion;
    Rigidbody rb;
    private bool wait = false;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.rigidbody.tag == "Monster" && !wait)
        {
            StartCoroutine(ParticleSpawn(collision.contacts[0].point));
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("okl");
        if (other.tag == "Monster" && !wait)
        {
            StartCoroutine(ParticleSpawn(rb.ClosestPointOnBounds(other.transform.position)));
        }
    }
    IEnumerator ParticleSpawn(Vector3 position)
    {
        Instantiate(explosion, position, Quaternion.identity);
        wait = true;
        yield return new WaitForSeconds(0.2f);
        wait = false;
    }
}
