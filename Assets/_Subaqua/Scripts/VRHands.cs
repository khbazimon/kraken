﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class VRHands : MonoBehaviour
{
    public Creature creature;
    public SteamVR_Behaviour_Pose controllerPose;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.rigidbody != null)
        {
            Vector3 movementSpeed = Vector3.zero;
            if(creature != null)
            {
                movementSpeed = creature.GetVelocity();
            }
            collision.rigidbody.velocity = controllerPose.GetVelocity() + movementSpeed;
            collision.rigidbody.angularVelocity = controllerPose.GetAngularVelocity();
        }
    }
}
