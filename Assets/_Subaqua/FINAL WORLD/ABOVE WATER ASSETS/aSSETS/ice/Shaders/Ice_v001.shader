// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:9361,x:34060,y:32456,varname:node_9361,prsc:2|diff-6170-RGB,diffpow-9815-OUT,spec-8746-OUT,gloss-7590-OUT,normal-958-OUT,emission-899-OUT,alpha-7929-OUT,refract-2819-OUT;n:type:ShaderForge.SFN_Color,id:6170,x:32907,y:32716,ptovrint:False,ptlb:colour,ptin:_colour,varname:node_6170,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9044118,c2:0.9619468,c3:1,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:499,x:32814,y:33094,ptovrint:False,ptlb:fresnel_Strength,ptin:_fresnel_Strength,varname:node_499,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Multiply,id:9894,x:32997,y:32927,varname:node_9894,prsc:2|A-8970-OUT,B-499-OUT;n:type:ShaderForge.SFN_Fresnel,id:8970,x:32802,y:32913,varname:node_8970,prsc:2|EXP-302-OUT;n:type:ShaderForge.SFN_Vector1,id:302,x:32626,y:32881,varname:node_302,prsc:2,v1:0;n:type:ShaderForge.SFN_Multiply,id:8571,x:33331,y:32832,varname:node_8571,prsc:2|A-9894-OUT,B-5293-RGB,C-9924-RGB;n:type:ShaderForge.SFN_Cubemap,id:5293,x:32997,y:33094,ptovrint:False,ptlb:cubemap,ptin:_cubemap,varname:node_5293,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:2507c0b2a5527b3418118962630a4951,pvfc:0;n:type:ShaderForge.SFN_Tex2d,id:9924,x:33027,y:33319,varname:node_9924,prsc:2,tex:8b49b3091e7a92041baf71b6ad41e927,ntxv:0,isnm:False|TEX-2235-TEX;n:type:ShaderForge.SFN_Clamp01,id:899,x:33561,y:32844,varname:node_899,prsc:2|IN-8571-OUT;n:type:ShaderForge.SFN_Slider,id:7929,x:33634,y:33023,ptovrint:False,ptlb:opacity,ptin:_opacity,varname:node_7929,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.9777086,max:1;n:type:ShaderForge.SFN_Tex2d,id:1283,x:32805,y:32368,varname:node_1283,prsc:2,tex:c7e2c5e2246354c4ba6e35bf1d8a38c0,ntxv:3,isnm:True|TEX-1819-TEX;n:type:ShaderForge.SFN_Multiply,id:3289,x:33014,y:32368,varname:node_3289,prsc:2|A-7922-OUT,B-1283-RGB;n:type:ShaderForge.SFN_Slider,id:7922,x:32668,y:32253,ptovrint:False,ptlb:Distortion strength,ptin:_Distortionstrength,varname:node_7922,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-2,cur:2,max:2;n:type:ShaderForge.SFN_ComponentMask,id:2819,x:33207,y:32471,varname:node_2819,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-3289-OUT;n:type:ShaderForge.SFN_Lerp,id:958,x:33584,y:32218,varname:node_958,prsc:2|A-9631-OUT,B-3289-OUT,T-4229-OUT;n:type:ShaderForge.SFN_Vector1,id:4229,x:33403,y:32321,varname:node_4229,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Vector3,id:9631,x:33365,y:32107,varname:node_9631,prsc:2,v1:0,v2:0,v3:1;n:type:ShaderForge.SFN_ValueProperty,id:8746,x:33753,y:32555,ptovrint:False,ptlb:Specular,ptin:_Specular,varname:node_8746,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.2;n:type:ShaderForge.SFN_Tex2dAsset,id:2235,x:32424,y:33335,ptovrint:False,ptlb:diffuse,ptin:_diffuse,varname:node_2235,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:8b49b3091e7a92041baf71b6ad41e927,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ValueProperty,id:7590,x:33532,y:32455,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_7590,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:9815,x:33842,y:32286,ptovrint:False,ptlb:diffusePower,ptin:_diffusePower,varname:node_9815,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:10;n:type:ShaderForge.SFN_TexCoord,id:3720,x:32251,y:32322,varname:node_3720,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Tex2dAsset,id:1819,x:32340,y:32567,ptovrint:False,ptlb:normal,ptin:_normal,varname:node_1819,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c7e2c5e2246354c4ba6e35bf1d8a38c0,ntxv:3,isnm:True;proporder:6170-499-5293-7929-7922-8746-2235-7590-9815-1819;pass:END;sub:END;*/

Shader "Shader Forge/ice" {
    Properties {
        _colour ("colour", Color) = (0.9044118,0.9619468,1,1)
        _fresnel_Strength ("fresnel_Strength", Float ) = 2
        _cubemap ("cubemap", Cube) = "_Skybox" {}
        _opacity ("opacity", Range(0, 1)) = 0.9777086
        _Distortionstrength ("Distortion strength", Range(-2, 2)) = 2
        _Specular ("Specular", Float ) = 0.2
        _diffuse ("diffuse", 2D) = "white" {}
        _Gloss ("Gloss", Float ) = 1
        _diffusePower ("diffusePower", Float ) = 10
        _normal ("normal", 2D) = "bump" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x xboxone ps4 psp2 n3ds wiiu 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _GrabTexture;
            uniform float4 _colour;
            uniform float _fresnel_Strength;
            uniform samplerCUBE _cubemap;
            uniform float _opacity;
            uniform float _Distortionstrength;
            uniform float _Specular;
            uniform sampler2D _diffuse; uniform float4 _diffuse_ST;
            uniform float _Gloss;
            uniform float _diffusePower;
            uniform sampler2D _normal; uniform float4 _normal_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 projPos : TEXCOORD5;
                UNITY_FOG_COORDS(6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 node_1283 = UnpackNormal(tex2D(_normal,TRANSFORM_TEX(i.uv0, _normal)));
                float3 node_3289 = (_Distortionstrength*node_1283.rgb);
                float3 normalLocal = lerp(float3(0,0,1),node_3289,0.1);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float2 sceneUVs = (i.projPos.xy / i.projPos.w) + node_3289.rg;
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float3 specularColor = float3(_Specular,_Specular,_Specular);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = pow(max( 0.0, NdotL), _diffusePower) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = _colour.rgb;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float4 node_9924 = tex2D(_diffuse,TRANSFORM_TEX(i.uv0, _diffuse));
                float3 emissive = saturate(((pow(1.0-max(0,dot(normalDirection, viewDirection)),0.0)*_fresnel_Strength)*texCUBE(_cubemap,viewReflectDirection).rgb*node_9924.rgb));
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,_opacity),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x xboxone ps4 psp2 n3ds wiiu 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _GrabTexture;
            uniform float4 _colour;
            uniform float _fresnel_Strength;
            uniform samplerCUBE _cubemap;
            uniform float _opacity;
            uniform float _Distortionstrength;
            uniform float _Specular;
            uniform sampler2D _diffuse; uniform float4 _diffuse_ST;
            uniform float _Gloss;
            uniform float _diffusePower;
            uniform sampler2D _normal; uniform float4 _normal_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 projPos : TEXCOORD5;
                LIGHTING_COORDS(6,7)
                UNITY_FOG_COORDS(8)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 node_1283 = UnpackNormal(tex2D(_normal,TRANSFORM_TEX(i.uv0, _normal)));
                float3 node_3289 = (_Distortionstrength*node_1283.rgb);
                float3 normalLocal = lerp(float3(0,0,1),node_3289,0.1);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float2 sceneUVs = (i.projPos.xy / i.projPos.w) + node_3289.rg;
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float3 specularColor = float3(_Specular,_Specular,_Specular);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = pow(max( 0.0, NdotL), _diffusePower) * attenColor;
                float3 diffuseColor = _colour.rgb;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * _opacity,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
