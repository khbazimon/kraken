// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:1012,x:34491,y:32919,varname:node_1012,prsc:2|emission-397-OUT,voffset-7723-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:2746,x:31545,y:32994,ptovrint:False,ptlb:mix,ptin:_mix,varname:_mix,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:27b3e5e645bb4dc4fb21a174fab813e4,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1173,x:32330,y:33483,varname:_node_1173,prsc:2,tex:27b3e5e645bb4dc4fb21a174fab813e4,ntxv:0,isnm:False|UVIN-4826-OUT,TEX-2746-TEX;n:type:ShaderForge.SFN_Multiply,id:4863,x:32585,y:33381,varname:node_4863,prsc:2|A-1173-R,B-6496-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6496,x:32330,y:33330,ptovrint:False,ptlb:vo_strength,ptin:_vo_strength,varname:node_6496,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Append,id:7723,x:32821,y:33368,varname:node_7723,prsc:2|A-8999-OUT,B-8999-OUT,C-4863-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8999,x:32534,y:33575,ptovrint:False,ptlb:node_8999,ptin:_node_8999,varname:node_8999,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_TexCoord,id:8520,x:30944,y:33101,varname:node_8520,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_ValueProperty,id:5867,x:30852,y:33457,ptovrint:False,ptlb:vo_speed_U,ptin:_vo_speed_U,varname:node_5867,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_ValueProperty,id:8382,x:30852,y:33593,ptovrint:False,ptlb:vo_speed_V,ptin:_vo_speed_V,varname:node_8382,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Append,id:971,x:31103,y:33501,varname:node_971,prsc:2|A-5867-OUT,B-8382-OUT;n:type:ShaderForge.SFN_Time,id:102,x:30831,y:32917,varname:node_102,prsc:2;n:type:ShaderForge.SFN_Multiply,id:8767,x:31427,y:33506,varname:node_8767,prsc:2|A-971-OUT,B-102-T;n:type:ShaderForge.SFN_Add,id:5963,x:31676,y:33506,varname:node_5963,prsc:2|A-8767-OUT,B-8520-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:9108,x:32096,y:32966,varname:node_9108,prsc:2,tex:27b3e5e645bb4dc4fb21a174fab813e4,ntxv:0,isnm:False|TEX-2746-TEX;n:type:ShaderForge.SFN_ValueProperty,id:5023,x:30487,y:32642,ptovrint:False,ptlb:lines_speed_U,ptin:_lines_speed_U,varname:_vo_speed_U_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.03;n:type:ShaderForge.SFN_ValueProperty,id:5360,x:30487,y:32737,ptovrint:False,ptlb:lines_speed_V,ptin:_lines_speed_V,varname:_vo_speed_V_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Append,id:829,x:30717,y:32680,varname:node_829,prsc:2|A-5023-OUT,B-5360-OUT;n:type:ShaderForge.SFN_Multiply,id:108,x:31079,y:32746,varname:node_108,prsc:2|A-829-OUT,B-102-T;n:type:ShaderForge.SFN_Add,id:2056,x:31376,y:32809,varname:node_2056,prsc:2|A-108-OUT,B-8520-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:7535,x:32096,y:32747,varname:node_7535,prsc:2,tex:27b3e5e645bb4dc4fb21a174fab813e4,ntxv:0,isnm:False|UVIN-5735-OUT,TEX-2746-TEX;n:type:ShaderForge.SFN_Add,id:9824,x:32433,y:32756,varname:node_9824,prsc:2|A-938-OUT,B-7535-B;n:type:ShaderForge.SFN_Multiply,id:3832,x:32909,y:32900,varname:node_3832,prsc:2|A-9316-OUT,B-9108-G;n:type:ShaderForge.SFN_ValueProperty,id:938,x:32231,y:32696,ptovrint:False,ptlb:line_add,ptin:_line_add,varname:node_938,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.8;n:type:ShaderForge.SFN_Append,id:8284,x:31686,y:33713,varname:node_8284,prsc:2|A-6979-OUT,B-1071-OUT;n:type:ShaderForge.SFN_Multiply,id:4826,x:32047,y:33604,varname:node_4826,prsc:2|A-8284-OUT,B-5963-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7879,x:30568,y:32183,ptovrint:False,ptlb:BigLines_speed_U,ptin:_BigLines_speed_U,varname:_lines_speed_U_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.04;n:type:ShaderForge.SFN_ValueProperty,id:7006,x:30568,y:32278,ptovrint:False,ptlb:BigLines_speed_V,ptin:_BigLines_speed_V,varname:_lines_speed_V_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Append,id:8813,x:30775,y:32213,varname:node_8813,prsc:2|A-7879-OUT,B-7006-OUT;n:type:ShaderForge.SFN_Multiply,id:9819,x:31118,y:32294,varname:node_9819,prsc:2|A-8813-OUT,B-102-T;n:type:ShaderForge.SFN_Add,id:3401,x:31444,y:32366,varname:node_3401,prsc:2|A-9819-OUT,B-8520-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:4647,x:32096,y:32547,varname:node_4647,prsc:2,tex:27b3e5e645bb4dc4fb21a174fab813e4,ntxv:0,isnm:False|UVIN-2734-OUT,TEX-2746-TEX;n:type:ShaderForge.SFN_Add,id:9316,x:32632,y:32622,varname:node_9316,prsc:2|A-229-OUT,B-9824-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1116,x:32192,y:32387,ptovrint:False,ptlb:bigLine_strength,ptin:_bigLine_strength,varname:node_1116,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Multiply,id:229,x:32435,y:32463,varname:node_229,prsc:2|A-1116-OUT,B-4647-A;n:type:ShaderForge.SFN_Multiply,id:2734,x:31787,y:32372,varname:node_2734,prsc:2|A-7866-OUT,B-3401-OUT;n:type:ShaderForge.SFN_Append,id:7866,x:31477,y:32180,varname:node_7866,prsc:2|A-7138-OUT,B-529-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7138,x:31274,y:32116,ptovrint:False,ptlb:bigLines_tile_U,ptin:_bigLines_tile_U,varname:node_7138,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:4;n:type:ShaderForge.SFN_ValueProperty,id:529,x:31274,y:32258,ptovrint:False,ptlb:bigLines_tile_V,ptin:_bigLines_tile_V,varname:node_529,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:328,x:31105,y:32587,ptovrint:False,ptlb:lines_tile_U,ptin:_lines_tile_U,varname:node_328,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3;n:type:ShaderForge.SFN_ValueProperty,id:4139,x:31105,y:32678,ptovrint:False,ptlb:lines_tile_V,ptin:_lines_tile_V,varname:node_4139,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Append,id:7256,x:31376,y:32637,varname:node_7256,prsc:2|A-328-OUT,B-4139-OUT;n:type:ShaderForge.SFN_Multiply,id:5735,x:31651,y:32732,varname:node_5735,prsc:2|A-7256-OUT,B-2056-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6979,x:31349,y:33698,ptovrint:False,ptlb:vo_tile_u,ptin:_vo_tile_u,varname:node_6979,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:1071,x:31334,y:33821,ptovrint:False,ptlb:vo_tile_v,ptin:_vo_tile_v,varname:node_1071,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Lerp,id:2687,x:33573,y:32779,varname:node_2687,prsc:2|A-9710-RGB,B-1459-RGB,T-3834-OUT;n:type:ShaderForge.SFN_Slider,id:9025,x:32743,y:32366,ptovrint:False,ptlb:colour_slider,ptin:_colour_slider,varname:node_9025,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:2;n:type:ShaderForge.SFN_Multiply,id:3834,x:33096,y:32630,varname:node_3834,prsc:2|A-9025-OUT,B-3832-OUT;n:type:ShaderForge.SFN_Color,id:9710,x:33281,y:32199,ptovrint:False,ptlb:Colour_1,ptin:_Colour_1,varname:node_9710,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.729,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:1459,x:33279,y:32410,ptovrint:False,ptlb:Colour_2,ptin:_Colour_2,varname:node_1459,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0.1310346,c4:1;n:type:ShaderForge.SFN_Multiply,id:675,x:33808,y:32936,varname:node_675,prsc:2|A-2687-OUT,B-3832-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7972,x:33907,y:33159,ptovrint:False,ptlb:Emissive_strength,ptin:_Emissive_strength,varname:node_7972,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.5;n:type:ShaderForge.SFN_Multiply,id:397,x:34101,y:32992,varname:node_397,prsc:2|A-675-OUT,B-7972-OUT;proporder:2746-6496-8999-5867-8382-5023-5360-938-7879-7006-1116-7138-529-328-4139-6979-1071-9025-9710-1459-7972;pass:END;sub:END;*/

Shader "Custom/NewSurfaceShader" {
    Properties {
        _mix ("mix", 2D) = "white" {}
        _vo_strength ("vo_strength", Float ) = 0.5
        _node_8999 ("node_8999", Float ) = 0
        _vo_speed_U ("vo_speed_U", Float ) = 0.1
        _vo_speed_V ("vo_speed_V", Float ) = 0
        _lines_speed_U ("lines_speed_U", Float ) = 0.03
        _lines_speed_V ("lines_speed_V", Float ) = 0
        _line_add ("line_add", Float ) = 0.8
        _BigLines_speed_U ("BigLines_speed_U", Float ) = 0.04
        _BigLines_speed_V ("BigLines_speed_V", Float ) = 0
        _bigLine_strength ("bigLine_strength", Float ) = 2
        _bigLines_tile_U ("bigLines_tile_U", Float ) = 4
        _bigLines_tile_V ("bigLines_tile_V", Float ) = 1
        _lines_tile_U ("lines_tile_U", Float ) = 3
        _lines_tile_V ("lines_tile_V", Float ) = 1
        _vo_tile_u ("vo_tile_u", Float ) = 0
        _vo_tile_v ("vo_tile_v", Float ) = 0
        _colour_slider ("colour_slider", Range(0, 2)) = 1
        _Colour_1 ("Colour_1", Color) = (0,0.729,1,1)
        _Colour_2 ("Colour_2", Color) = (0,1,0.1310346,1)
        _Emissive_strength ("Emissive_strength", Float ) = 1.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _mix; uniform float4 _mix_ST;
            uniform float _vo_strength;
            uniform float _node_8999;
            uniform float _vo_speed_U;
            uniform float _vo_speed_V;
            uniform float _lines_speed_U;
            uniform float _lines_speed_V;
            uniform float _line_add;
            uniform float _BigLines_speed_U;
            uniform float _BigLines_speed_V;
            uniform float _bigLine_strength;
            uniform float _bigLines_tile_U;
            uniform float _bigLines_tile_V;
            uniform float _lines_tile_U;
            uniform float _lines_tile_V;
            uniform float _vo_tile_u;
            uniform float _vo_tile_v;
            uniform float _colour_slider;
            uniform float4 _Colour_1;
            uniform float4 _Colour_2;
            uniform float _Emissive_strength;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float4 node_102 = _Time + _TimeEditor;
                float2 node_4826 = (float2(_vo_tile_u,_vo_tile_v)*((float2(_vo_speed_U,_vo_speed_V)*node_102.g)+o.uv0));
                float4 _node_1173 = tex2Dlod(_mix,float4(TRANSFORM_TEX(node_4826, _mix),0.0,0));
                v.vertex.xyz += float3(_node_8999,_node_8999,(_node_1173.r*_vo_strength));
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 node_102 = _Time + _TimeEditor;
                float2 node_2734 = (float2(_bigLines_tile_U,_bigLines_tile_V)*((float2(_BigLines_speed_U,_BigLines_speed_V)*node_102.g)+i.uv0));
                float4 node_4647 = tex2D(_mix,TRANSFORM_TEX(node_2734, _mix));
                float2 node_5735 = (float2(_lines_tile_U,_lines_tile_V)*((float2(_lines_speed_U,_lines_speed_V)*node_102.g)+i.uv0));
                float4 node_7535 = tex2D(_mix,TRANSFORM_TEX(node_5735, _mix));
                float4 node_9108 = tex2D(_mix,TRANSFORM_TEX(i.uv0, _mix));
                float node_3832 = (((_bigLine_strength*node_4647.a)+(_line_add+node_7535.b))*node_9108.g);
                float3 emissive = ((lerp(_Colour_1.rgb,_Colour_2.rgb,(_colour_slider*node_3832))*node_3832)*_Emissive_strength);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _mix; uniform float4 _mix_ST;
            uniform float _vo_strength;
            uniform float _node_8999;
            uniform float _vo_speed_U;
            uniform float _vo_speed_V;
            uniform float _vo_tile_u;
            uniform float _vo_tile_v;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float4 node_102 = _Time + _TimeEditor;
                float2 node_4826 = (float2(_vo_tile_u,_vo_tile_v)*((float2(_vo_speed_U,_vo_speed_V)*node_102.g)+o.uv0));
                float4 _node_1173 = tex2Dlod(_mix,float4(TRANSFORM_TEX(node_4826, _mix),0.0,0));
                v.vertex.xyz += float3(_node_8999,_node_8999,(_node_1173.r*_vo_strength));
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
