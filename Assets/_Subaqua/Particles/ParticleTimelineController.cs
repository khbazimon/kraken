﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleTimelineController : MonoBehaviour
{
    public bool disabled = false;

    // Update is called once per frame
    void Update()
    {
        if (disabled)
        {
            GetComponent<ParticleSystem>().Stop();
        }
    }
}
